﻿# Gather-InterestingFileHashes.ps1
# 
# Scans the hard disk for interesting files and stores their paths, sizes, modification times, and hashes in a SQLite database.
# 

# Path to our database file and the System.Data.SQLite provider DLL
param (
    [string]$dbPath,
    [string]$sqliteDLLpath,
    [string]$jsonLogDir
)
Set-PSDebug -strict; $ErrorActionPreference = "SilentlyContinue"; $DebugPreference = "SilentlyContinue"; $VerbosePreference = "Continue"

$jsonFile = Join-Path -Path $jsonLogDir -ChildPath "FileHashes-$((Get-Date).ToString('s')).json"

# What file extensions are we interested in?
$interestingTypes = @('*.exe','*.dll','*.sys','*.com','*.ps1','*.psm1','*.bat','*.cmd','*.scr','*.doc','*.docx','*.xls','*.xlsx','*.ppt','*.pptx',
                      '*.inf','*.vbs','*.reg','*.vb','*.vbe','*.wsh','*.wsf','*.class','*.jar','*.pl','*.pm','*.py','*.pyc','*.pyo','*.pyw','*.crx',
                      '*.xpi','*.sh','*.csg','*.zsh','*.bash','*.ksh','*.kext','*.so') 


# Verify that the JSON log output directory exists, make it if it doesn't
if (-not (Test-Path -Path $jsonLogDir)) {
    New-Item -Path $jsonLogDir -ItemType Directory
}

# Load the System.Data.SQLite provider
$DLLName = "System.Data.SQLite.dll"
try {
    Write-Verbose "Attempting to load System.Data.SQLite.dll from $sqliteDLLpath\$DLLName"
    Add-Type -Path (Join-Path -Path $sqliteDLLpath -ChildPath $DLLName)
}
Catch {
    Write-Warning "System.Data.SQLite provider not found or not loadable."
    exit;
}
$con = New-Object -TypeName System.Data.SQLite.SQLiteConnection
Write-Verbose "Attempting to connect to SQLite database at $dbpath"
$con.ConnectionString = "Data Source=$dbpath"
$con.Open()

# Set up SQLite database to hold results, if it hasn't already been created
$sql = $con.CreateCommand()
$sql.CommandText = "
create table if not exists hashes(md5 varchar(32),
                    sha1 varchar(40),
                    sha256 varchar(64),
                    path varchar(1024) primary key,
                    size int,
                    modified varchar(20));
create table if not exists metadata(scandate varchar(20),
                                    numfiles int);
"
try {
    
    $success = $sql.ExecuteNonQuery()
}
catch {
    Write-Warning "Unable to create SQLite tables."
    Write-Error $Error[0]
    $con.Close()
    exit;
}

# Load previous results from SQLite table
$interestingFiles = @{}
$sql = $con.CreateCommand()
$sql.CommandText = "SELECT * FROM hashes;"
$adapter = New-Object -TypeName System.Data.SQLite.SQLiteDataAdapter $sql
$data = New-Object System.Data.DataSet
[void]$adapter.Fill($data)
if ($data.tables[0].Rows.Count -gt 0) {
    (0..($data.tables[0].Rows.Count - 1)) | ForEach {
        $obj = $data.tables[0].Rows[$_]
        $interestingFiles.Add($obj.path, (New-Object psobject -Property @{ SHA1 = $obj.sha1; SHA256 = $obj.sha256; MD5 = $obj.md5; SIZE = $obj.size; MODIFIED = $obj.modified }))
    }
}

# Load previous scan date from SQLite table
$sql = $con.CreateCommand()
$lastscan = (Get-Date "1900-01-01T00:00:00")
$previousFiles = 0
$sql.CommandText = "SELECT scandate,numfiles FROM metadata ORDER BY scandate DESC LIMIT 1"
$adapter = New-Object -TypeName System.Data.SQLite.SQLiteDataAdapter $sql
$data = New-Object System.Data.DataSet
[void]$adapter.Fill($data)
if ($data.tables[0].Rows.Count -gt 0) {
    (0..($data.tables[0].Rows.Count - 1)) | ForEach {
        $obj = $data.tables[0].Rows[$_]
        $lastscan = (Get-Date($obj.scandate))
        $previousFiles = $obj.numfiles
    }
}

if ($lastscan -ne (Get-Date "1900-01-01T00:00:00")) {
    Write-Verbose "Last scan was $($lastscan.ToString('s')) and resulted in $previousFiles files."
}

# Scan drive for new interesting files or those with changed size or modification time. Recompute all hashes for any such file
$scantime = (Get-Date).ToString('s')
Get-ChildItem -Path 'c:\' -Recurse -Force -Include $interestingTypes | ForEach-Object {
    $modified = $_.LastWriteTimeUtc.ToString('s')
    $size = $_.Length
    $update = $True
    if ($interestingFiles.($_.FullName)) {
        $i = $interestingFiles.($_.FullName)
        if ($modified -eq $i.MODIFIED -and $size -eq $i.SIZE) {
               $update = $False
        }
    }
    if ($update) {
        $fileObj = New-Object psobject -Property @{ SHA1 = ($_ | Get-FileHash -Algorithm SHA1).Hash;
                                                    SHA256 = ($_ | Get-FileHash -Algorithm SHA256).Hash;
                                                    MD5 = ($_ | Get-FileHash -Algorithm MD5).Hash;
                                                    MODIFIED = $modified;
                                                    SIZE = $size;
                                                    INCLUDE = $True }
        Write-Debug "Calculated hashes of file $($_.FullName)."
        if ($interestingFiles.($_.FullName)) {
            $interestingFiles.($_.FullName) = $fileObj
        } else {
            $interestingFiles.Add($_.FullName, $fileObj)
        }
    }
            
}

# Drop SQLite hashes table and rebuild with new data
$sql = $con.CreateCommand()
$sql.CommandText = "DROP TABLE hashes"
try {
    
    $success = $sql.ExecuteNonQuery()
}
catch {
    Write-Warning "Unable to drop old SQLite hashes table."
    Write-Error $Error[0]
    exit;
}
$sql = $con.CreateCommand()
$sql.CommandText = "
create table if not exists hashes(md5 varchar(32),
                    sha1 varchar(40),
                    sha256 varchar(64),
                    path varchar(1024) primary key,
                    size int,
                    modified varchar(20));
"
try {
    
    $success = $sql.ExecuteNonQuery()
}
catch {
    Write-Warning "Unable to create new SQLite table."
    Write-Error $Error[0]
    $con.Close()
    exit;
}

# Dump the full hash data set into the SQLite table
$sql = $con.CreateCommand()
$sql.CommandText = 'begin transaction'
$success = $sql.ExecuteNonQuery()

$interestingFiles.GetEnumerator() | ForEach-Object {
    # Dump this record as JSON to the per-run updated file hash log
    if ($_.Value.INCLUDE) {
        $outObj = New-Object PSObject -Property @{ path   = $_.key;
                                                   MD5    = $_.Value.MD5;
                                                   SHA1   = $_.Value.SHA1;
                                                   SHA256 = $_.Value.SHA256;
                                                   size   = $_.Value.SIZE;
                                                   modified = $_.Value.MODIFIED }

        $outObj | ConvertTo-Json -Compress | Out-File -FilePath $jsonFile -Append -Encoding utf8
    }

    # Add this record to the SQLite database
    $sql = $con.CreateCommand()
    $sql.CommandText = '
insert into hashes (md5,sha1,sha256,path,size,modified) VALUES (@md5,@sha1,@sha256,@path,@size,@modified)
    '
    $null = $sql.Parameters.AddWithValue("@md5", $_.Value.MD5)
    $null = $sql.Parameters.AddWithValue("@sha1", $_.Value.SHA1)
    $null = $sql.Parameters.AddWithValue("@sha256", $_.Value.SHA256)
    $null = $sql.Parameters.AddWithValue("@path", $_.key)
    $null = $sql.Parameters.AddWithValue("@size", $_.Value.SIZE)
    $null = $sql.Parameters.AddWithValue("@modified", $_.Value.MODIFIED)
    Try {
        Write-Debug "$($_.key) MD5:$($_.Value.MD5) SHA1:$($_.Value.SHA1) SHA256:$($_.Value.SHA256) SIZE:$($_.Value.SIZE) MOD:$($_.Value.MODIFIED)"
        $success = $sql.ExecuteNonQuery()}
    Catch {
        Write-Warning 'An insert failed!'
        Write-Error $Error[0]
        $sql.CommandText = 'rollback transaction'
        $success = $sql.ExecuteNonQuery()
        Remove-Item -Path $jsonFile
        exit;
    }
}
$sql.CommandText = 'commit transaction'
$success = $sql.ExecuteNonQuery()

#Record datetime of this scan in the SQLite metadata table
$sql = $con.CreateCommand()
$sql.CommandText = '
insert into metadata (scandate,numfiles) VALUES (@scandate,@numfiles)
'
$null = $sql.Parameters.AddWithValue("@scandate", $scantime)
$null = $sql.Parameters.AddWithValue("@numfiles", $interestingFiles.Count)
Try { 
    $success = $sql.ExecuteNonQuery()
} Catch { 
    Write-Warning 'Failed to record scan date and number of files'
    Write-Error $Error[0]
}

$con.Close()

Write-Verbose "This scan detected $($interestingFiles.Count) files, a net change of $($interestingFiles.Count - $previousFiles)"